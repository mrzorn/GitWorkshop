﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomSprite : MonoBehaviour {

	[SerializeField]
	private Sprite[] _sprites;

	// Use this for initialization
	void Start () {
		if (_sprites != null && _sprites.Length > 0)
			GetComponent<SpriteRenderer> ().sprite = _sprites [Random.Range (0, _sprites.Length)];
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
