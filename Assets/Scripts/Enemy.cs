﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

	[SerializeField]
	private Transform _explosionPrefab;

	private float startY;

	//variables for the size and shape of our sine wave
	private float wavelength = 2f;
	private float amplitude = 2f;
	private float speed = 5f;


	//this will be the amount of damage that it takes to kill the enemy
	//we can increase this number to make them harder to kill
	private int hp = 1;


	void Start () 
	{
		//get the intitial y position
		startY = transform.position.y;

	}
	
	void Update () 
	{
		MoveEnemy ();

	}

	void MoveEnemy(){
		//unless its dead, move it in a sine wave
		if (hp > 0) {
			Vector2 pos = transform.position;
			pos.x -= speed * Time.deltaTime;
			//we need to do some math to find out there we should be on the vertical part of the sine wave
			pos.y = Mathf.Sin (pos.x / wavelength) * amplitude + startY;
			
			transform.position = pos;
		}
	}

	void OnTriggerEnter2D(Collider2D other) {

		//if the enemy is dead, don't do anything
		if (hp <= 0)
			return;

		//check to see if what it hit was a bullet, if so, deal damage and destroy the bullet
		Bullet bullet = other.GetComponent<Bullet> ();

		if (bullet != null) {
			TakeDamage (bullet.GetDamage ());
			Destroy (bullet.gameObject);
		} else {

			//if you collided with the player, then do something to the player
			Player player = other.GetComponent<Player>();

			if(player != null){
				TakeDamage(hp);
				player.CollidedWithEnemy (this);
			}

		}
	}

	void TakeDamage(int amount){
		//subtract the damage amount from the enemie's hp
		hp -= amount;

		//if out of hp, die
		if (hp <= 0) {
			Die ();
		}
	}
	
	void Die(){
		hp = 0;

		Transform.Instantiate (_explosionPrefab, transform.position, Quaternion.identity);

		Destroy (gameObject);
	}
}
