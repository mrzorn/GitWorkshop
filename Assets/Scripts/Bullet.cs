﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour {

	//the amount of damage that this bullet will do
	//we can increase this number to make it more powerful
	private int damage = 1;

	private float speed = 15f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		MoveRight ();
	}

	void MoveRight(){
		//keep moving to the right
		Vector2 pos = transform.position;
		pos.x += speed * Time.deltaTime;
		
		transform.position = pos;
	}

	public int GetDamage(){
		//return the amount of damage that this bullet will do
		return damage;
	}
}
